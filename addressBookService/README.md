### Description
	Address-Book Service
	Acceptance Criteria
	As a Reece Branch Manager user can perform operations
	Address book should hold name and phone numbers of contact entries
	Users should be able to add a new contact entry to an existing address book
	Users should be able to remove an existing contact entry from an existing address book
	Users should be able to retireve all contacts in an address book
	Users should be able to retireve a unique set of all contacts across multiple address books

### Soultion through Integration test case 
	com.reece.addressbook.controller.ContactControllerIntegeratonTest covers all the acceptance test cases.
	As a part of requirement the crud operations of contacts in address-book was based Admin role.
	User 'test' has a ADMIN role to perform CRUD operations.
	The integration test also covering authentication and autherization use case. 
	
	

### Solution and Test Coverage 
	This Application has been made as possible as close to prod env.
	The application has more then 95% code test coverage . The seed data has been supplied as part of development.
![alt text](coverage.JPG?raw=true "Output")



#### Technology stack used
	a. Spring Data with JPA 2.0 Specifications
	b. Swagger API documentation with authentication header.
	c. Liquibase for managing the database change-log and provides the seed data through csv files. addressBook.csv and contact.csv
	d. Logback for logging framework
	e. In memory H2 database.
	h. Maven for build process.
	e. Spring basic authentication and autherization.

## Build and Run application

### Prerequisites
	Java 1.8
	Apache Maven (3x)

### Build
	mvn clean install
### Test case execution
	mvn test

### To run application from terminal
	java -jar target/addressBook-1.0.0.jar (ensure that application is already build using 'mvn clean install')

### REST Soultion using Swagger API
	http://localhost:8080/swagger-ui.html
	credentials: test/password 
	Role: Admin
	Sample data: AddressBookId 1,2
				 ContactId 1,2,3,4,5,6
![alt text](swagger-api.JPG?raw=true "Output")

	If you want to see the DB structure
### H2-database In memory DB:
	http://localhost:8080/h2-console
	JDBC URL:jdbc:h2:mem:test_adrs_book
	Username: sa
	password: password
![alt text](h2-console.JPG?raw=true "Output")

### In case you are using other client/Postman

	header: authorization
	value: Basic dGVzdDpwYXNzd29yZA==
	or in case it prompt for credentials: test/password 
![alt text](postman.JPG?raw=true "Output")

Feel free to reach me on contacttosanjeev@gmail.com if you find any issue to run this application.