package com.reece.addressbook.error;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;



@ControllerAdvice
public class ExceptionTranslator {

    /**
     * Post-process the Problem payload to add the message key for the front-end if needed.
     */
    
    @ExceptionHandler(BadRequestAlertException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public @ResponseBody ErrorResponse badRequestAlert(BadRequestAlertException ex, HttpServletResponse response, HttpServletRequest request) throws IOException {
        return new ErrorResponse(ex.getMessage(), ex.getField(), request.getRequestURI());
    }
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ErrorResponse internalServerError(Exception ex, HttpServletResponse response, HttpServletRequest request) throws IOException {
        return new ErrorResponse(ex.getMessage(), "Internal Server Error.", request.getRequestURI());
    }
}
