package com.reece.addressbook.error;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String field;

    private final String message;
    
    private final String requestUrl;

    public ErrorResponse(String message, String field, String url) {
        this.field = field;
        this.message = message;
        this.requestUrl = url;
    }


    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
    
    public String getRequestUrl() {
        return requestUrl;
    }
 

}
