package com.reece.addressbook.error;


public class BadRequestAlertException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String field;

    public BadRequestAlertException(String message,String field) {
        super(message);
        this.field = field;
    }
    
    public String getField() {
        return field;
    }
}
