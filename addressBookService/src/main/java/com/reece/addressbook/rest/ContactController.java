package com.reece.addressbook.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reece.addressbook.model.Contact;
import com.reece.addressbook.service.ContactService;

@RestController
@RequestMapping("/api/v1/address-books/")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("{addressBookId}/contacts")
    public Contact createContactToAddressBook(@PathVariable String addressBookId, @Valid @RequestBody Contact newContact) {
        return contactService.addContact(addressBookId, newContact);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("{addressBookId}/contacts/{contactId}")
    public void deleteContactFromAddressBook(@PathVariable String addressBookId,@PathVariable String contactId) {
        contactService.removeContact(addressBookId, contactId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("{addressBookId}/contacts")
    public List<Contact> getContactsFromAddressBook(@PathVariable String addressBookId) {
        return contactService.getAllContactsFromAddressBook(addressBookId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/contacts")
    public List<Contact> getAllUniqueContactsAddressBooks(@RequestParam(value = "unique", defaultValue = "false") Boolean unique) {
        return contactService.getAllUniqueContacts(unique);
    }

}
