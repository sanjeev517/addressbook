package com.reece.addressbook.repo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.reece.addressbook.model.Contact;


/**
 * Spring Data  repository for the Contact entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    
    @Query(value = "SELECT DISTINCT FIRST_NAME, LAST_NAME, PHONE_NUMBER  FROM CONTACT", nativeQuery = true)
    List<Object> getDistinctRecords();
}
