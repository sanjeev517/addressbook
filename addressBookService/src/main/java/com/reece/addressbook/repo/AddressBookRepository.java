package com.reece.addressbook.repo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reece.addressbook.model.AddressBook;


/**
 * Spring Data  repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressBookRepository extends JpaRepository<AddressBook, Long> {

	List<AddressBook> findAllByRegion(String region);
}
