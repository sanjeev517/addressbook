package com.reece.addressbook.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.reece.addressbook.error.BadRequestAlertException;
import com.reece.addressbook.model.AddressBook;
import com.reece.addressbook.model.Contact;
import com.reece.addressbook.repo.AddressBookRepository;
import com.reece.addressbook.repo.ContactRepository;

@Service
@Transactional
public class ContactService {

    @Autowired
    private AddressBookRepository addressBookRepository;

    @Autowired
    private ContactRepository contactRepository;

    public Contact addContact(String addressBookId, Contact contact) {
        AddressBook addressBook = addressBookRepository.findById(Long.parseLong(addressBookId)).orElseThrow(() -> new BadRequestAlertException("AddressBook with the given Id does not exist", addressBookId));
        addressBook.getContacts().add(contact);
        contact.setAddressBook(addressBook);
        return contactRepository.save(contact);
    }

    public void removeContact(String addressBookId, final String contactId) {
        Contact contact = contactRepository.findById(Long.parseLong(contactId))
                .orElseThrow(() -> new BadRequestAlertException("Contact with the given Id does not exist", contactId));
        if (!contact.getAddressBook().getId().toString().equals(addressBookId)) {
            throw new BadRequestAlertException("Contact doesn't belong to given address book.", addressBookId);
        }
        contactRepository.delete(contact);
    }

    public List<Contact> getAllContactsFromAddressBook(String addressBookId) {
        AddressBook addressBook = addressBookRepository.findById(Long.parseLong(addressBookId)).orElseThrow(() -> new BadRequestAlertException("AddressBook with the given Id does not exist", addressBookId));
        return addressBook.getContacts();
    }

    public List<Contact> getAllUniqueContacts(boolean uinque) {
        if (uinque) {
            List<Object> all = contactRepository.getDistinctRecords();
            return all.stream()
                    .map(a -> new Contact(((Object[]) a)[0].toString(), ((Object[]) a)[1].toString(), ((Object[]) a)[2].toString()))
                    .collect(Collectors.toList());
        } else {
            return contactRepository.findAll();
        }

    }

}
