package com.reece.addressbook.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.reece.addressbook.error.BadRequestAlertException;
import com.reece.addressbook.model.AddressBook;
import com.reece.addressbook.model.Contact;
import com.reece.addressbook.repo.AddressBookRepository;
import com.reece.addressbook.repo.ContactRepository;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

    @InjectMocks
    private ContactService contactService;

    @Mock
    private ContactRepository contactRepository;
    
    @Mock
    private AddressBookRepository addressBookRepository;
    
    @Test
    public void testAddContact_expect_success() {
        AddressBook ab = new AddressBook();
        ab.setId(1l);
        ab.setRegion("test");
        Mockito.when(addressBookRepository.findById(Mockito.any())).thenReturn(Optional.of(ab));
        Contact contact2 = new Contact("xyz","abc","234567");
        contact2.setId(234l);
        Mockito.when(contactRepository.save(Mockito.any())).thenReturn(contact2);
        
        Contact result = contactService.addContact("1", new Contact("xyz","abc","234567"));
        assertThat(result.getFirstName()).isEqualTo("xyz");
        assertThat(result.getLastName()).isEqualTo("abc");
        assertThat(result.getId()).isEqualTo(234l);
        assertThat(result.getPhoneNumbers()).isEqualTo("234567");
    }
    
    @Test(expected=BadRequestAlertException.class)
    public void testAddContact_expect_exception() {
        Mockito.when(addressBookRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
        contactService.addContact("1", new Contact("xyz","abc","234567"));
    }
    
    @Test(expected=Test.None.class)
    public void testRemoveContact_expect_success() {
        Contact c = new Contact("xyz","abc","234567");
        c.setId(234l);
        
        AddressBook ab = new AddressBook();
        ab.setId(1l);
        ab.setRegion("test");
        ab.getContacts().add(c);
        c.setAddressBook(ab);
        Mockito.when(contactRepository.findById(Mockito.any())).thenReturn(Optional.of(c));
        Mockito.doNothing().when(contactRepository).delete(Mockito.any());
        contactService.removeContact("1", "234");
        
    }
    
    @Test(expected=BadRequestAlertException.class)
    public void testRemoveContact_expect_exception() {
        Mockito.when(contactRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
        contactService.removeContact("1", "234");
    }
    
    @Test
    public void testGetAllContactsFromAddressBook_expect_success(){
        Contact[] contacts = {new Contact("xyz","abc","234567"),new Contact("test1","test2","67676")};
        
        AddressBook ab = new AddressBook();
        ab.setId(1l);
        ab.setRegion("test");
        int id=1;
        for (Contact contact : contacts) {
        contact.setId(new Long(id++)); 
        ab.getContacts().add(contact);
        }
        
        Mockito.when(addressBookRepository.findById(1l)).thenReturn(Optional.of(ab));
        List<Contact> result = contactService.getAllContactsFromAddressBook("1");
        assertThat(result.get(0).getFirstName()).isEqualTo("xyz");
        assertThat(result.get(0).getLastName()).isEqualTo("abc");
        assertThat(result.get(0).getPhoneNumbers()).isEqualTo("234567");
        assertThat(result.get(0).getId()).isEqualTo(1l);
        
        assertThat(result.get(1).getFirstName()).isEqualTo("test1");
        assertThat(result.get(1).getLastName()).isEqualTo("test2");
        assertThat(result.get(1).getPhoneNumbers()).isEqualTo("67676");
        assertThat(result.get(1).getId()).isEqualTo(2l);
    }
    
    @Test(expected=BadRequestAlertException.class)
    public void testGetAllContactsFromAddressBook_expect_exception(){
        Mockito.when(addressBookRepository.findById(1l)).thenReturn(Optional.ofNullable(null));
        contactService.getAllContactsFromAddressBook("1");
    }
    
    @Test
    public void testGetAllUniqueContacts_expect_success() {
        String[][] contacts = {{"xyz","abc","234567"},{"test1","test2","67676"}};
        
        Mockito.when(contactRepository.getDistinctRecords()).thenReturn(Arrays.asList(contacts));
        
        List<Contact> result = contactService.getAllUniqueContacts(true);
        assertThat(result.get(0).getFirstName()).isEqualTo("xyz");
        assertThat(result.get(0).getLastName()).isEqualTo("abc");
        assertThat(result.get(0).getPhoneNumbers()).isEqualTo("234567");
        
        assertThat(result.get(1).getFirstName()).isEqualTo("test1");
        assertThat(result.get(1).getLastName()).isEqualTo("test2");
        assertThat(result.get(1).getPhoneNumbers()).isEqualTo("67676");
    }
    
    @Test
    public void testGetNonUniqueContacts__expect_success() {
        Contact[] contacts = new Contact[]{new Contact("xyz","abc","234567"),new Contact("test1","test2","67676"), new Contact("test1","test2","67676")};
        Mockito.when(contactRepository.findAll()).thenReturn(Lists.newArrayList(contacts));
        
        List<Contact> result = contactService.getAllUniqueContacts(false);
        assertThat(result.get(0).getFirstName()).isEqualTo("xyz");
        assertThat(result.get(0).getLastName()).isEqualTo("abc");
        assertThat(result.get(0).getPhoneNumbers()).isEqualTo("234567");
        
        assertThat(result.get(1).getFirstName()).isEqualTo("test1");
        assertThat(result.get(1).getLastName()).isEqualTo("test2");
        assertThat(result.get(1).getPhoneNumbers()).isEqualTo("67676");
        
        assertThat(result.get(2).getFirstName()).isEqualTo("test1");
        assertThat(result.get(2).getLastName()).isEqualTo("test2");
        assertThat(result.get(2).getPhoneNumbers()).isEqualTo("67676");
    }
    
    
}


