package com.reece.addressbook.repo;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ContactRepositoryTest {

    @Autowired
    private ContactRepository contactRepo;

    @Test
    public void getDistinctRecordsTest() {
        String[][] records = {{"Foo", "Bar", "45454"},
                        {"James", "Gilchrist", "656554"},
                        {"Martin", "Wiz", "67676"},
                        {"Mike", "Parson", "354435"},
                        {"Sanjeev", "verma", "12344"}};
        List<Object> distincts = contactRepo.getDistinctRecords();
        Assertions.assertThat(distincts.size()).isEqualTo(5);
        for (int i = 0; i < distincts.size(); i++) {
            Assertions.assertThat(records[i][0]).isEqualTo(((Object[]) distincts.get(i))[0].toString());
            Assertions.assertThat(records[i][1]).isEqualTo(((Object[]) distincts.get(i))[1].toString());
            Assertions.assertThat(records[i][2]).isEqualTo(((Object[]) distincts.get(i))[2].toString());
        }
    }
}
