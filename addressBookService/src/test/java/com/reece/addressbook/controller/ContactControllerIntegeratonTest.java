package com.reece.addressbook.controller;


import static com.jayway.jsonpath.JsonPath.read;
import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.Charset;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import com.reece.addressbook.Application;
import com.reece.addressbook.model.Contact;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = {"classpath:application-test.properties"})
public class ContactControllerIntegeratonTest {

    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void performAllIntegrationTest_expect_success() {
        
        verifygetContactsFromAddressBook();
        
        // creating a new contact.
        verifyCreateContact();
       
        verifyUniqueContacts();
        
        verifyAllContacts();
        
        verifyDeleteContact();
    }

    private void verifyDeleteContact() {
        ResponseEntity<String> response;
        response = restTemplate.exchange(createURLWithPort("/api/v1/address-books/1/contacts/2"),HttpMethod.DELETE, new HttpEntity<String>(createAuthHeaders()), String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }

    private void verifyAllContacts() {
        ResponseEntity<String> response;
        String reponseBody;
        response = restTemplate.exchange(createURLWithPort("/api/v1/address-books/contacts?unique=false"),HttpMethod.GET, new HttpEntity<String>(createAuthHeaders()), String.class);
        reponseBody = response.getBody();
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(StringUtils.countOccurrencesOf(reponseBody, "Sanjeev")).isEqualTo(2);
        assertThat(StringUtils.countOccurrencesOf(reponseBody, "verma")).isEqualTo(2);
    }

    private void verifyUniqueContacts() {
        ResponseEntity<String> response;
        String reponseBody;
        response = restTemplate.exchange(createURLWithPort("/api/v1/address-books/contacts?unique=true"),HttpMethod.GET, new HttpEntity<String>(createAuthHeaders()), String.class);
        reponseBody = response.getBody();
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(StringUtils.countOccurrencesOf(reponseBody, "Sanjeev")).isEqualTo(1);
        assertThat(StringUtils.countOccurrencesOf(reponseBody, "verma")).isEqualTo(1);
    }

    private void verifyCreateContact() {
        ResponseEntity<String> response;
        Contact contact = new Contact("xyz","abc","234567");
//        response = restTemplate.po(createURLWithPort("/api/v1/address-books/1/contacts"),contact, String.class);
        HttpEntity entity = new HttpEntity(contact,createAuthHeaders());
        
        response = restTemplate.exchange(createURLWithPort("/api/v1/address-books/1/contacts"),HttpMethod.POST,entity , String.class);
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }

    private void verifygetContactsFromAddressBook() {
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/api/v1/address-books/1/contacts"),HttpMethod.GET, new HttpEntity<String>(createAuthHeaders()), String.class);
         
        String reponseBody = response.getBody();
        
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(read(reponseBody, "$.length()").toString()).isEqualTo("2");
        
        assertThat(read(reponseBody, "$.[0].id").toString()).isEqualTo("1");
        assertThat(read(reponseBody, "$.[0].firstName").toString()).isEqualTo("Sanjeev");
        assertThat(read(reponseBody, "$.[0].lastName").toString()).isEqualTo("verma");
        assertThat(read(reponseBody, "$.[0].phoneNumbers").toString()).isEqualTo("12344");
        
        assertThat(read(reponseBody, "$.[1].id").toString()).isEqualTo("2");
        assertThat(read(reponseBody, "$.[1].firstName").toString()).isEqualTo("Foo");
        assertThat(read(reponseBody, "$.[1].lastName").toString()).isEqualTo("Bar");
        assertThat(read(reponseBody, "$.[1].phoneNumbers").toString()).isEqualTo("45454");
    }
    
    @Test
    public void getAllContactsFromAddressBook_expect_404_and_verify_error_response() {
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/api/v1/address-books/5/contacts"),
                HttpMethod.GET, new HttpEntity<String>(createAuthHeaders()), String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode().value());
        String reponseBody = response.getBody();
        assertThat(read(reponseBody, "$.field").toString()).isEqualTo("5");
        assertThat(read(reponseBody, "$.message").toString()).isEqualTo("AddressBook with the given Id does not exist");
        assertThat(read(reponseBody, "$.requestUrl").toString()).isEqualTo("/api/v1/address-books/5/contacts");
    }

    @Test
    public void getAllContactsFromAddressBook_expect_400_and_verify_internal_server_error() {
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/api/v1/address-books/nonNumeric/contacts"),
                HttpMethod.GET, new HttpEntity<String>(createAuthHeaders()), String.class);
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatusCode().value());
        String reponseBody = response.getBody();
        assertThat(read(reponseBody, "$.message").toString()).isEqualTo("For input string: \"nonNumeric\"");
        assertThat(read(reponseBody, "$.field").toString()).isEqualTo("Internal Server Error.");
        assertThat(read(reponseBody, "$.requestUrl").toString()).isEqualTo("/api/v1/address-books/nonNumeric/contacts");
    }

    private HttpHeaders createAuthHeaders(){
        String username="test";
        String password ="password";
        return new HttpHeaders() {{
              String auth = username + ":" + password;
              byte[] encodedAuth = Base64.encodeBase64( 
                 auth.getBytes(Charset.forName("US-ASCII")) );
              String authHeader = "Basic " + new String( encodedAuth );
              set( "Authorization", authHeader );
           }};
     }
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
