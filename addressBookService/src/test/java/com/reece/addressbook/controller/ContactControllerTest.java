package com.reece.addressbook.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.reece.addressbook.error.BadRequestAlertException;
import com.reece.addressbook.model.Contact;
import com.reece.addressbook.rest.ContactController;
import com.reece.addressbook.service.ContactService;

@RunWith(MockitoJUnitRunner.class)
public class ContactControllerTest {

    @InjectMocks
    private ContactController contactController;
    
    @Mock
    private ContactService contactService;
    
    @Test
    public void testCreateContactToAddressBook_success() {
        Contact contact = new Contact("xyz","abc","234567");
        Contact contact2 = new Contact("xyz","abc","234567");
        contact2.setId(234l);
        Mockito.when(contactService.addContact("1", contact)).thenReturn(contact2);
        Contact result = contactController.createContactToAddressBook("1", contact);
        assertThat(result.getFirstName()).isEqualTo("xyz");
        assertThat(result.getLastName()).isEqualTo("abc");
        assertThat(result.getId()).isEqualTo(234l);
        assertThat(result.getPhoneNumbers()).isEqualTo("234567");
    }

    @Test(expected=BadRequestAlertException.class)
    public void createContactToAddressBookTest_error_expected() {
        Contact contact = new Contact("xyz","abc","234567");
        Mockito.when(contactService.addContact("1", contact)).thenThrow(BadRequestAlertException.class);
        contactController.createContactToAddressBook("1", contact);
    }

    @Test(expected=Test.None.class)
    public void testDeleteContactFromAddressBook_success() {
        Mockito.doNothing().when(contactService).removeContact("1", "1");
        contactController.deleteContactFromAddressBook("1", "1");
    }

    @Test(expected=BadRequestAlertException.class)
    public void testDeleteContactFromAddressBook_throw_error() {
        Mockito.doThrow(BadRequestAlertException.class).when(contactService).removeContact("1", "1");
        contactController.deleteContactFromAddressBook("1", "1");
    }
    
    @Test
    public void testGetContactsFromAddressBook_success() {
        Contact[] contacts = {new Contact("xyz","abc","234567"),new Contact("test1","test2","67676")};
        Mockito.when(contactService.getAllContactsFromAddressBook("1")).thenReturn(Arrays.asList(contacts));
        List<Contact> result = contactController.getContactsFromAddressBook("1");
        
        assertThat(result.get(0).getFirstName()).isEqualTo("xyz");
        assertThat(result.get(0).getLastName()).isEqualTo("abc");
        assertThat(result.get(0).getPhoneNumbers()).isEqualTo("234567");
        
        assertThat(result.get(1).getFirstName()).isEqualTo("test1");
        assertThat(result.get(1).getLastName()).isEqualTo("test2");
        assertThat(result.get(1).getPhoneNumbers()).isEqualTo("67676");
    }

    @Test(expected=BadRequestAlertException.class)
    public void testContactsFromAddressBook_throw_error() {
        Mockito.when(contactService.getAllContactsFromAddressBook("1")).thenThrow(BadRequestAlertException.class);
        contactController.getContactsFromAddressBook("1");
    }
    
    
    @Test
    public void testGetAllUniqueContactsAddressBooks_expect_success() {
        Contact c1 = new Contact("xyz","abc","234567");
        Contact c2 = new Contact("lmn","rtyt","1234567");
        List<Contact> contacts = Arrays.asList(new Contact[]{c1,c2});
        Mockito.when(contactService.getAllUniqueContacts(true)).thenReturn(contacts);
        List<Contact> result = contactController.getAllUniqueContactsAddressBooks(true);
        
        assertThat(result.get(0).getFirstName()).isEqualTo("xyz");
        assertThat(result.get(0).getLastName()).isEqualTo("abc");
        assertThat(result.get(0).getPhoneNumbers()).isEqualTo("234567");
        
        assertThat(result.get(1).getFirstName()).isEqualTo("lmn");
        assertThat(result.get(1).getLastName()).isEqualTo("rtyt");
        assertThat(result.get(1).getPhoneNumbers()).isEqualTo("1234567");
    }


    @Test(expected=BadRequestAlertException.class)
    public void testGetAllUniqueContactsAddressBooks_expect_exception() {
        Mockito.when(contactService.getAllUniqueContacts(true)).thenThrow(BadRequestAlertException.class);
        contactController.getAllUniqueContactsAddressBooks(true);
    }
    
}

